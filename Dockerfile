FROM python:3.7-alpine

ENV PYTHONUNBUFFERED 1
ENV ROOT_DIR /usr/src

WORKDIR $ROOT_DIR

RUN set -ex \
    && addgroup -g 82 -S www-data \
    && adduser -u 82 -D -S -G www-data www-data \
    && apk add --upgrade --no-cache --virtual .build-deps \
        openssl-dev \
    && apk add --upgrade --virtual \
        build-base \
        gcc \
        g++ \
        libffi-dev \
        libxml2 \
        libc-dev \
        musl-dev \
        python3-dev \
        py-cffi \
    && apk --no-cache add --repository http://dl-cdn.alpinelinux.org/alpine/edge/testing watchman

COPY . $ROOT_DIR

RUN set -ex \
    && pip install --upgrade pip \
    && pip install -r $ROOT_DIR/requirements.txt --no-cache-dir \
    && apk del --no-cache .build-deps \
    && chown -R www-data:www-data $ROOT_DIR

ENTRYPOINT ["python3", "/usr/src/app.py"]