import time
import pandas as pd
import argparse

from pathlib import Path
from typing import Dict, Any, List, Union, Generator
from geopy.distance import distance


def get_cities_cords_file(file_path: str) -> Generator:
    with open(file_path, 'r') as f:
        for line in f:
            city, *arg, cords = line.split(';')
            lang, long = cords.strip().split(',')
            yield {'city': city.strip(), 'lang_long': (float(lang), float(long))}


def cities_pairs(file_path: str) -> Generator:
    for val in get_cities_cords_file(file_path):
        for val2 in get_cities_cords_file(file_path):
            if val['city'] != val2['city']:
                yield {'city1': val['city'], 'lang_long1': val['lang_long'], 'city2': val2['city'], 'lang_long2': val2['lang_long']}


def calculate_distance_kilometers():
    memo = {}
    def wrapper(row: Any) -> Union[int, float]:
        key = row['lang_long1'] + row['lang_long2']
        if key in memo:
            return memo[key]

        key2 = row['lang_long2'] + row['lang_long1']
        if key2 in memo:
            return memo[key2]

        memo[key] = distance(row['lang_long1'], row['lang_long2']).kilometers
        memo[key2] = memo[key]
        return memo[key]
    return wrapper


def processing_cities(data: List[Dict]) -> None:
    df = pd.DataFrame(data=data)
    df['distance_kilometers'] = df.apply(calculate_distance_kilometers(), axis=1)
    df.to_csv(f'cities-pairs-distance.csv')


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--file_path', nargs='?', const=1, type=Path, default='cc.csv')
    p, unknown = parser.parse_known_args()
    start_time = time.time()
    data_gen = [cities for cities in cities_pairs(p.file_path)]
    print(f'--- cities_pairs --- {(time.time() - start_time)} seconds ---')
    processing_cities(data_gen)
    print(f'--- processing_cities --- {(time.time() - start_time)} seconds ---')
